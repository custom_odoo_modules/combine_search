# -*- coding: utf-8 -*-
{
    'name': "Комбинированный поиск",
    'summary': "combine search",
    'description': "Расширяет стандартный поиск вебсайта",
    'author': "Xorikita",
    'website': "http://www.yourcompany.com",
    'category': 'Website/Website',
    'version': '0.1',
    'sequence' : 2,
    'depends': ['website'],
    'data': [
        'views\search_page.xml'
    ],
    'installable':True,
    'application':True,
    'auto_install':False,
    'assets': {
        'web.assets_frontend': [
            # ('prepend','/hr_employee_website/src/scss/employees.scss'),
            # ('prepend','/hr_employee_website/src/scss/employee.scss'),
            # '/hr_employee_website/src/js/get_employee.js',
            # '/hr_employee_website/src/js/create_lead_employee.js',
        ]
    }
}